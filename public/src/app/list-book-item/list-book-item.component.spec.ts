import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ListBookItemComponent } from './list-book-item.component';

describe('ListBookItemComponent', () => {
  let component: ListBookItemComponent;
  let fixture: ComponentFixture<ListBookItemComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ListBookItemComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ListBookItemComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
