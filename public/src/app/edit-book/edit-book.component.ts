import { Component, OnInit } from '@angular/core';
import { Book } from '../book';
import { Router, ActivatedRoute, ParamMap} from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-edit-book',
  templateUrl: './edit-book.component.html',
  styleUrls: ['./edit-book.component.css']
})

export class EditBookComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private bookService: BooksService) { }

  public book: Book;
  public copy: Book;
  public index: number;

  ngOnInit() {
      this.route.paramMap.subscribe((params: ParamMap) => {
      const  index = parseInt(params.get('id'), 10);
      this.book = this.bookService.getBook(index);
      console.log(this.book);
      this.index = index;
      this.copy = new Book(this.book.title, this.book.author, this.book.isbnNo, this.book.desc);
    });
  }

  saveBook() {
    this.router.navigate(['/listBooks']);
  }

  gotoHome() {
    this.router.navigate(['/home']);
  }

  cancel() {
    const books = this.bookService.getBooks();
    books[this.index] = this.copy;
    this.router.navigate(['/listBooks']);
  }

}
