import { Injectable } from '@angular/core';
import { Book } from './book';

@Injectable({
  providedIn: 'root'
})

export class BooksService {

  constructor() { }

  public books = [];

 getBooks() {
  return this.books;
}

getBook(index: number): Book {
  console.log(this.books[index]);
  return this.books[index];
}

addBook(book) {
  this.books.push(book);

}

deleteBook(index) {
  this.books.splice(index, 1);
}

}
