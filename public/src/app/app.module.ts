import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppComponent } from './app.component';
import { FormsModule } from '@angular/forms';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { BooksService } from './books.service';
import { HomeComponent } from './home/home.component';
import { ListBookItemComponent } from './list-book-item/list-book-item.component';

@NgModule({
  declarations:
  [
    AppComponent,
    routingComponents,
    HomeComponent,
    ListBookItemComponent
  ],

  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule
  ],

  providers: [BooksService],
  bootstrap: [AppComponent]
})

export class AppModule { }
