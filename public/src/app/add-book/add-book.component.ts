import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { BooksService } from '../books.service';

@Component({
  selector: 'app-add-book',
  templateUrl: './add-book.component.html',
  styleUrls: ['./add-book.component.css']
})

export class AddBookComponent  {

  constructor(
    private router: Router,
    private bookService: BooksService) { }

  addBook(book) {
      if ( book.isbnNo === '' ||
          book.title === '' ||
          book.author === '' ||
          book.desc === '') {
        alert('all fields are mandatory');
        return false;
      }
    this.bookService.addBook(book);
    this.router.navigate(['/listBooks']);
  }

  gotoHome() {
    this.router.navigate(['/home']);
  }

}
